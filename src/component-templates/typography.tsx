import { Typography } from '@material-ui/core';
import React from 'react';

const Txt: React.FC = ({children, ...props}) => <Typography {...props}>{children}</Typography>;

export default Txt;