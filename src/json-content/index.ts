import { Attributes } from "react";

type JsonContent = {
    name: string,
    props?: Attributes | null,
};

export default JsonContent;
