import React from 'react';
import './App.css';
import RenderComponent from './rendering-engine/render';

function App() {
  return (
    <div className="App">
      {RenderComponent({name:'foo'})}
      {RenderComponent({
          name:'button', 
          props:{
            color:'primary',
            variant: 'contained',
            children:'Button Component!',
          },
      })}
      {RenderComponent({
          name:'text', 
          props:{
            children:'Typography component!',
          },
      })}
    </div>
  );
}

export default App;
